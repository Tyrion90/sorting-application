package be.ld.sorting.options;

public class ArgumentHelper {
    public static SortingApplicationOptions findOptions(String[] args) {
        //-d --double --Double -D
        //-s --string --String -S
        //-i --integer --Integer -I

        // -n=XXXX array size
        // -r=XXXX number of runs

        SortingApplicationOptions.Type type = null;
        Integer size = null;
        Integer runs = null;
        boolean verbose = false;
        for (String arg : args) {
            String lowercaseArgs = arg.toLowerCase().strip();
            if (lowercaseArgs.equals("--verbose")) {
                verbose = true;
            } else if (lowercaseArgs.matches("-[a-z]") || lowercaseArgs.matches("--[a-z]+")) {
                type = parseTypeArgument(lowercaseArgs);
            } else if (lowercaseArgs.matches("-n:\\d+")) {
                size = parseNumberFromArgument(arg);
            } else if (lowercaseArgs.matches("-r:\\d+")) {
                runs = parseNumberFromArgument(arg);
            } else {
                System.out.println("Failed to parse argument " + arg);
            }
        }

        return new SortingApplicationOptions(type, size, runs, verbose);
    }

    private static Integer parseNumberFromArgument(String arg) {
        if (!arg.matches("-[a-zA-Z]:\\d+")) {
            System.out.println("Failed to parse argument " + arg);
            return null;
        }

        String numberPart = arg.substring(arg.indexOf(':') + 1);
        return Integer.parseInt(numberPart);
    }

    private static SortingApplicationOptions.Type parseTypeArgument(String arg) {
        return switch (arg) {
            case "-d", "--double" -> SortingApplicationOptions.Type.DOUBLE;
            case "-s", "--string" -> SortingApplicationOptions.Type.STRING;
            case "-i", "--integer" -> SortingApplicationOptions.Type.INTEGER;
            default -> {
                System.out.println("Failed to parse argument " + arg);
                yield null;
            }
        };
    }

}
