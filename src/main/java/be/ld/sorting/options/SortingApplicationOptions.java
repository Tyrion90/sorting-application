package be.ld.sorting.options;

public record SortingApplicationOptions(Type type, Integer arraySize, Integer runs, boolean verbose) {
    public static final Type DEFAULT_TYPE = Type.INTEGER;
    public static final int DEFAULT_ARRAY_SIZE = 1000;
    public static final int DEFAULT_NUMBER_RUNS = 100;

    public SortingApplicationOptions(Type type, Integer arraySize, Integer runs, boolean verbose) {
        this.type = type == null ? DEFAULT_TYPE : type;
        this.arraySize = arraySize == null ? DEFAULT_ARRAY_SIZE : arraySize;
        this.runs = runs == null ? DEFAULT_NUMBER_RUNS : runs;
        this.verbose = verbose;
    }

    public enum Type {
        INTEGER,
        DOUBLE,
        STRING
    }
}
