package be.ld.sorting.result;

import java.util.Arrays;

public record SortingResult<E extends Comparable<E>>
        (
                E[] collection,
                long comparisons,
                long swaps,
                long elapsedTime,
                String algorithmName
        ) {


    @Override
    public String toString() {
        return String.format("Algorithm: %20s - Comparisons: %,9d - Swaps: %,9d - Elapsed Time: %,9d μs", algorithmName, comparisons, swaps, elapsedTime / 1000);
    }

    public String printArray() {
        return Arrays.toString(collection);
    }

    public boolean isSorted() {
        boolean sorted = true;
        for(int i = 1; i < this.collection.length; i++){
            if(this.collection[i-1].compareTo(this.collection[i]) > 0 ){
                sorted = false;
                break;
            }
        }
        return sorted;
    }
}
