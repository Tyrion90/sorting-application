package be.ld.sorting.result;

import java.util.Comparator;
import java.util.List;
import java.util.LongSummaryStatistics;

public class AggregateSortingResult<E extends Comparable<E>> {
    private final String PRINT_FORMAT = "avg: %,12.2f (low: %,9d, high: %,9d)";
    private final String PRINT_TIME_FORMAT = "avg: %,9.2fμs (low: %,9.2fμs, high: %,9.2fμs)";
    private final LongSummaryStatistics comparisons;
    private final LongSummaryStatistics swaps;

    private final LongSummaryStatistics elapsedTime;
    private final String algorithmName;
    private final boolean sorted;


    public AggregateSortingResult(List<SortingResult<E>> results) {
        this.sorted = results.stream().allMatch(SortingResult::isSorted);
        if (!this.sorted) {
            throw new RuntimeException("Not all results are correctly sorted in " + results.getFirst().algorithmName());
        }
        this.algorithmName = results.getFirst().algorithmName();
        this.swaps = results.stream().mapToLong(SortingResult::swaps).summaryStatistics();
        this.comparisons = results.stream().mapToLong(SortingResult::comparisons).summaryStatistics();
        this.elapsedTime = results.stream().mapToLong(SortingResult::elapsedTime).summaryStatistics();
    }

    @Override
    public String toString() {
        return String.format("%-16s - Elapsed Time: %s - Comps: %s - Swaps: %s", algorithmName, printElapsedtime(), printComparisons(), printSwaps());
    }

    public String printComparisons() {
        return String.format(PRINT_FORMAT, this.comparisons.getAverage(), this.comparisons.getMin(), this.comparisons.getMax());
    }

    public String printSwaps() {
        return String.format(PRINT_FORMAT, this.swaps.getAverage(), this.swaps.getMin(), this.swaps.getMax());
    }

    public String printElapsedtime() {
        return String.format(PRINT_TIME_FORMAT, this.elapsedTime.getAverage() / 1000D, this.elapsedTime.getMin() / 1000D, this.elapsedTime.getMax() / 1000D);
    }

    public LongSummaryStatistics getElapsedTime() {
        return elapsedTime;
    }

    public boolean isSorted() {
        return this.sorted;
    }

    public static Comparator<AggregateSortingResult<?>> avarageElapsedTimeComparator() {
        return Comparator.comparingDouble(res -> res.elapsedTime.getAverage());
    }

    public static Comparator<AggregateSortingResult<?>> averageComparisonsComparator() {
        return Comparator.comparingDouble(res -> res.comparisons.getAverage());
    }

    public static Comparator<AggregateSortingResult<?>> averageSwapsComparator() {
        return Comparator.comparingDouble(res -> res.swaps.getAverage());
    }


}
