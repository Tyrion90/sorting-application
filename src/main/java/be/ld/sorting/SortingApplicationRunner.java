package be.ld.sorting;

import be.ld.sorting.options.ArgumentHelper;
import be.ld.sorting.options.SortingApplicationOptions;
import be.ld.sorting.randomgenerators.RandomDoubleGenerator;
import be.ld.sorting.randomgenerators.RandomIntegerGenerator;
import be.ld.sorting.randomgenerators.RandomStringGenerator;

public class SortingApplicationRunner {
    public static void main(String[] args) {
        SortingApplicationOptions options = ArgumentHelper.findOptions(args);
        SortingApplication<?> app = getSortingApplication(options.type());

        app.runSortingBenchmarks(options);
    }

    private static SortingApplication<?> getSortingApplication(SortingApplicationOptions.Type type) {
        return switch (type) {
            case DOUBLE -> {
                System.out.println("Created application for sorting Doubles");
                yield new SortingApplication<>(new RandomDoubleGenerator());
            }
            case STRING -> {
                System.out.println("Created application for sorting Strings");
                yield new SortingApplication<>(new RandomStringGenerator());
            }
            case INTEGER -> {
                System.out.println("Created application for sorting Integers");
                yield new SortingApplication<>(new RandomIntegerGenerator());
            }
        };
    }
}
