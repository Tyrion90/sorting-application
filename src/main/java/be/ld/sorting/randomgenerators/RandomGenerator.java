package be.ld.sorting.randomgenerators;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.IntFunction;
import java.util.stream.Stream;

public abstract class RandomGenerator<E extends Comparable<E>> {

    public static final StreamOptions NO_DUPLICATES_UNORDERED = new StreamOptions(false, false, false);
    public static final StreamOptions NO_DUPLICATES_ORDERED = new StreamOptions(false, true, false);
    public static final StreamOptions NO_DUPLICATES_REVERSED = new StreamOptions(false, true, true);
    public static final StreamOptions DUPLICATES_UNORDERED = new StreamOptions(true, false, false);
    public static final StreamOptions DUPLICATES_ORDERED = new StreamOptions(true, true, false);
    public static final StreamOptions DUPLICATES_REVERSED = new StreamOptions(true, true, true);

    protected abstract Stream<E> randomStream(int ARRAY_SIZE);

    protected abstract IntFunction<E[]> arraySupplier();

    public E[] generateRandomArray(StreamOptions options, int ARRAY_SIZE) {
        Stream<E> stream = randomStream(ARRAY_SIZE);

        E[] array;
        if (options.duplicatesAllowed()) {
            array = stream.limit(ARRAY_SIZE).toArray(arraySupplier());
        } else {
            array = stream.distinct().limit(ARRAY_SIZE).toArray(arraySupplier());
        }

        if (options.ordered()) {
            Comparator<E> comparator = options.reversed() ? Comparator.reverseOrder() : Comparator.naturalOrder();
            Arrays.sort(array, comparator);
        }

        return array;
    }

    public E[] generateRandomArray(int ARRAY_SIZE) {
        return generateRandomArray(DUPLICATES_UNORDERED, ARRAY_SIZE);
    }

    public record StreamOptions(boolean duplicatesAllowed, boolean ordered, boolean reversed) {
        public StreamOptions {
            if (reversed && !ordered) {
                throw new IllegalArgumentException("Reversing an array has no meaning if the array is unordered");
            }
        }
    }
}
