package be.ld.sorting.randomgenerators;

import java.util.Random;
import java.util.function.IntFunction;
import java.util.stream.Stream;

public class RandomIntegerGenerator extends RandomGenerator<Integer> {
    @Override
    public Integer[] generateRandomArray(StreamOptions options, int ARRAY_SIZE) {

        return super.generateRandomArray(options, ARRAY_SIZE);
//        return new Integer[]{9, 4, 7, 8, 8, 7, 5, 8, 4, 9};
//        return new Integer[]{1,2,3,4,4,4,5};
//        return IntStream.rangeClosed(1, 9).boxed().toArray(Integer[]::new);
    }

    @Override
    public Stream<Integer> randomStream(int ARRAY_SIZE) {
        Random rand = new Random();
        return Stream.generate(() -> rand.nextInt(0, 5 * ARRAY_SIZE));
    }

    @Override
    public IntFunction<Integer[]> arraySupplier() {
        return Integer[]::new;
    }
}
