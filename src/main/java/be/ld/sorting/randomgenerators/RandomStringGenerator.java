package be.ld.sorting.randomgenerators;

import java.util.Random;
import java.util.function.IntFunction;
import java.util.function.IntPredicate;
import java.util.stream.Stream;

public class RandomStringGenerator extends RandomGenerator<String> {
    private final CharacterSelection characterSelection;

    private static final int LENGTH_MIN = 2;
    private static final int LENGTH_MAX = 10;

    public static final CharacterSelection ALPHANUMERIC = new CharacterSelection(true, true, true);
    public static final CharacterSelection LETTERS_ONLY = new CharacterSelection(false, true, true);
    public static final CharacterSelection NUMBERS_ONLY = new CharacterSelection(true, false, false);
    public static final CharacterSelection UPPERCASE_ONLY = new CharacterSelection(false, true, false);

    public RandomStringGenerator() {
        this(ALPHANUMERIC);
    }

    public RandomStringGenerator(CharacterSelection characterSelection) {
        this.characterSelection = characterSelection;
    }

    @Override
    protected Stream<String> randomStream(int ARRAY_SIZE) {
        return Stream.generate(this::randomString);
    }

    private String randomString() {
        int leftLimit = 48;
        int rightLimit = 122;
        Random rand = new Random();

        int length = rand.nextInt(LENGTH_MIN, LENGTH_MAX + 1);

        return rand.ints(leftLimit, rightLimit + 1)
                .filter(this.characterSelection.createCharacterFilter())
                .limit(length)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    @Override
    protected IntFunction<String[]> arraySupplier() {
        return String[]::new;
    }

    public record CharacterSelection(boolean numbers, boolean uppercase, boolean lowercase) {

        private IntPredicate createCharacterFilter() {
            IntPredicate numberFilter = i -> i >= 48 && i <= 57; //ASCII codepoints 48 (number 0) to 57 (number 9)
            IntPredicate uppercaseFilter = i -> i >= 65 && i <= 90; //ASCII codepoints 65 (letter A) to 90 (letter Z)
            IntPredicate lowercaseFilter = i -> i >= 97 && i <= 122; //ASCII codepoints 97 (letter a) to 122 (letter z)

            IntPredicate result = i -> false;
            if (this.numbers())
                result = result.or(numberFilter);
            if (this.uppercase())
                result = result.or(uppercaseFilter);
            if (this.lowercase())
                result = result.or(lowercaseFilter);

            return result;
        }
    }
}
