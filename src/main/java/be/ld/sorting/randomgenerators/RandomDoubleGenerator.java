package be.ld.sorting.randomgenerators;

import java.util.Random;
import java.util.function.IntFunction;
import java.util.stream.Stream;

public class RandomDoubleGenerator extends RandomGenerator<Double> {
    @Override
    protected Stream<Double> randomStream(int ARRAY_SIZE) {
        Random rand = new Random();
        return Stream.generate(rand::nextDouble);
    }

    @Override
    protected IntFunction<Double[]> arraySupplier() {
        return Double[]::new;
    }
}
