package be.ld.sorting;

import be.ld.sorting.algorithm.*;
import be.ld.sorting.options.SortingApplicationOptions;
import be.ld.sorting.randomgenerators.RandomGenerator;
import be.ld.sorting.result.AggregateSortingResult;
import be.ld.sorting.result.SortingResult;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static be.ld.sorting.randomgenerators.RandomGenerator.*;


public class SortingApplication<E extends Comparable<E>> {
    private final RandomGenerator<E> randomGenerator;
    private final List<SortingAlgorithm<E>> algorithms;

    public SortingApplication(RandomGenerator<E> randomGenerator) {
        this.randomGenerator = randomGenerator;

        this.algorithms = List.of(
                new JavaSort<>(),
                new BubbleSort<>(),
                new SelectionSort<>(),
                new InsertionSort<>(),
                new ShellSort<>(),
                new QuickSort<>(),
                new CocktailShakerSort<>(),
                new MergeSort<>()
        );
    }

    public void runSortingBenchmarks(SortingApplicationOptions options) {
        performSingleSorting(options);
        sortingSortedArray(options.arraySize());
        sortingSortedArrayNoDuplicates(options.arraySize());
        sortingReverseSortedArray(options.arraySize());
        sortingReverseSortedArrayNoDuplicates(options.arraySize());

        performMultipleSortings(options);
    }


    private void performSorting(E[] arrayToSort) {
        performSorting(arrayToSort, false);
    }

    private void performSorting(E[] arrayToSort, boolean verbose) {
        System.out.printf("Running %s sorting algorithms on an array of size %s\n", algorithms.size(), arrayToSort.length);
        if (verbose) {
            System.out.println("Original array: " + Arrays.toString(arrayToSort));
        }

        List<SortingResult<E>> results = algorithms
                .stream()
                .map(al -> al.getSortedResult(arrayToSort)).toList();

        results.forEach(res -> {
            System.out.println(res + "  " + (res.isSorted() ? "Sorted" : "NOT sorted!!"));
            if (verbose) {
                System.out.println(res.printArray());
            }
        });
    }

    private void performSingleSorting(SortingApplicationOptions options) {
        E[] randomArray = randomGenerator.generateRandomArray(DUPLICATES_UNORDERED, options.arraySize());

        System.out.println("Sorting a random array");
        performSorting(randomArray, options.verbose());
    }

    private void sortingSortedArray(int arraySize) {
        E[] sortedArray = randomGenerator.generateRandomArray(DUPLICATES_ORDERED, arraySize);

        System.out.println("Sorting an already sorted array with duplicates");
        performSorting(sortedArray);
    }

    private void sortingSortedArrayNoDuplicates(int arraySize) {
        E[] sortedArray = randomGenerator.generateRandomArray(NO_DUPLICATES_ORDERED, arraySize);

        System.out.println("Sorting an already sorted array without duplicates");
        performSorting(sortedArray);
    }

    private void sortingReverseSortedArray(int arraySize) {
        E[] reverseSortedArray = randomGenerator.generateRandomArray(DUPLICATES_REVERSED, arraySize);

        System.out.println("Sorting a reverse sorted list with duplicates");
        performSorting(reverseSortedArray);
    }

    private void sortingReverseSortedArrayNoDuplicates(int arraySize) {
        E[] reverseSortedArray = randomGenerator.generateRandomArray(NO_DUPLICATES_REVERSED, arraySize);

        System.out.println("Sorting a reverse sorted list without duplicates");
        performSorting(reverseSortedArray);
    }

    private void performMultipleSortings(SortingApplicationOptions options) {
        System.out.println("Running " + options.runs() + " runs on random arrays of size " + options.arraySize());

        Supplier<E[]> supplier = () -> this.randomGenerator.generateRandomArray(options.arraySize());

        Map<String, List<SortingResult<E>>> resultsmap = algorithms.stream()
                .flatMap(
                        algorithm -> Stream
                                .generate(supplier)
                                .limit(options.runs())
                                .map(algorithm::getSortedResult)
                )
                .collect(Collectors.groupingBy(SortingResult::algorithmName));

        this.printResults(resultsmap.values());
    }

    private void printResults(Collection<List<SortingResult<E>>> results) {
        Stream<AggregateSortingResult<E>> averages = results
                .stream()
                .map(AggregateSortingResult::new)
                .sorted(AggregateSortingResult.avarageElapsedTimeComparator());

        averages.forEach(System.out::println);
    }

}