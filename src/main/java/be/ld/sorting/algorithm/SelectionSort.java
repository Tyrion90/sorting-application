package be.ld.sorting.algorithm;

public class SelectionSort<E extends Comparable<E>> extends SortingAlgorithm<E> {
    @Override
    public void sortCollection() {

        for (int i = 0; i < this.collection.length; i++) {
            int indexOfMinimum = i;
            for (int j = i; j < this.collection.length; j++) {
                if (isGreaterThan(indexOfMinimum, j)) {
                    indexOfMinimum = j;
                }
            }
            if (indexOfMinimum != i) {
                swap(i, indexOfMinimum);
            }
        }

    }

    @Override
    public String getName() {
        return "Selection sort";
    }
}
