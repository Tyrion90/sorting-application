package be.ld.sorting.algorithm;

import java.util.Arrays;

public class MergeSort<E extends Comparable<E>> extends SortingAlgorithm<E> {
    @Override
    public void sortCollection() {
        topDownMergeSort(this.collection);
    }

    private void topDownMergeSort(E[] arrayA) {
        E[] arrayB = Arrays.copyOf(arrayA, arrayA.length);
        this.swaps += arrayA.length;
        topDownSplitMerge(arrayA, 0, arrayA.length, arrayB);
    }

    private void topDownSplitMerge(E[] arrayB, int indexBegin, int indexEnd, E[] arrayA) {
        if (indexEnd - indexBegin <= 1) {
            return;
        }
        int indexMiddle = (indexEnd + indexBegin) / 2;
        topDownSplitMerge(arrayA, indexBegin, indexMiddle, arrayB);
        topDownSplitMerge(arrayA, indexMiddle, indexEnd, arrayB);
        topDownMerge(arrayB, indexBegin, indexMiddle, indexEnd, arrayA);
    }

    private void topDownMerge(E[] arrayB, int indexBegin,int indexMiddle, int indexEnd, E[] arrayA) {
        int i = indexBegin, j = indexMiddle;

        for (int k = indexBegin; k < indexEnd; k++) {
            if (i < indexMiddle && (j >= indexEnd || isLessThanOrEqual(arrayA, i, j))) {
                arrayB[k] = arrayA[i];
                i++;
            } else {
                arrayB[k] = arrayA[j];
                j++;
            }
            this.swaps++;
        }

    }

    protected boolean isLessThanOrEqual(E[] array, int indexFirst, int indexSecond) {
        this.comparisons++;
        return array[indexFirst].compareTo(array[indexSecond]) <= 0;
    }

    @Override
    public String getName() {
        return "Merge sort";
    }
}
