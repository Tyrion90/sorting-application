package be.ld.sorting.algorithm;

import java.util.Arrays;

public class JavaSort<E extends Comparable<E>> extends SortingAlgorithm<E> {
    @Override
    public void sortCollection() {
        Arrays.sort(this.collection);
    }

    @Override
    public String getName() {
        return "Java built-in";
    }
}
