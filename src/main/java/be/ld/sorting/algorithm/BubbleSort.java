package be.ld.sorting.algorithm;

public class BubbleSort<E extends Comparable<E>> extends SortingAlgorithm<E> {
    @Override
    public void sortCollection() {
        for (int i = 0; i < collection.length - 1; i++) {
            for (int j = 0; j < collection.length -1 - i; j++) {
                if (isGreaterThan(j, j + 1)) {
                    swap(j, j + 1);
                }
            }
        }
    }

    @Override
    public String getName() {
        return "Bubble sort";
    }
}
