package be.ld.sorting.algorithm;

public class CocktailShakerSort<E extends Comparable<E>> extends SortingAlgorithm<E> {
    @Override
    protected void sortCollection() {
        int beginIndex = 0;
        int endIndex = this.collection.length - 1;

        while (beginIndex < endIndex) {
            for (int i = beginIndex; i < endIndex; i++) {
                if (isGreaterThan(i, i + 1)) {
                    swap(i, i + 1);
                }
            }
            endIndex--;

            for (int i = endIndex; i > beginIndex; i--) {
                if (isLessThan(i, i - 1)) {
                    swap(i, i - 1);
                }
            }
            beginIndex++;
        }

    }

    @Override
    public String getName() {
        return "Cocktail sort";
    }
}
