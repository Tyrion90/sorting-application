package be.ld.sorting.algorithm;

public class QuickSort<E extends Comparable<E>> extends SortingAlgorithm<E> {
    @Override
    public void sortCollection() {
        quicksort(0, this.collection.length - 1);
    }

    private void quicksort(int indexLow, int indexHigh) {
        if (indexLow >= 0 && indexHigh >= 0 && indexLow < indexHigh) {
            int pivot = partition(indexLow, indexHigh);
            quicksort(indexLow, pivot);
            quicksort(pivot + 1, indexHigh);
        }
    }

    private int partition(int indexLow, int indexHigh) {
        int pivotIndex = choosePivot(indexLow, indexHigh);

        int leftIndex = indexLow - 1;
        int rightIndex = indexHigh + 1;

        while (leftIndex < rightIndex) {
            do {
                leftIndex++;
            } while (isLessThan(leftIndex, pivotIndex));

            do {
                rightIndex--;
            } while (isGreaterThan(rightIndex, pivotIndex));

            if (leftIndex < rightIndex) {
                swap(leftIndex, rightIndex);
                pivotIndex = swapPivot(pivotIndex, leftIndex, rightIndex);
            }
        }
        return rightIndex;
    }

    private int choosePivot(int indexLow, int indexHigh) {
        return (indexLow + indexHigh) / 2;
    }

    private int swapPivot(int pivot, int left, int right) {
        if (pivot == left) {
            return right;
        }
        if (pivot == right) {
            return left;
        }
        return pivot;
    }

    @Override
    public String getName() {
        return "Quicksort";
    }
}
