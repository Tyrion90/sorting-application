package be.ld.sorting.algorithm;

import be.ld.sorting.result.SortingResult;

import java.util.Arrays;

public abstract class SortingAlgorithm<E extends Comparable<E>> {
    protected E[] collection;
    protected long comparisons;
    protected long swaps;

    public SortingResult<E> getSortedResult(E[] input) {
        this.collection = Arrays.copyOf(input, input.length);
        this.comparisons = 0;
        this.swaps = 0;

        long startTime = System.nanoTime();

        this.sortCollection();

        long endTime = System.nanoTime();
        return new SortingResult<>(this.collection, comparisons, swaps, endTime - startTime, getName());
    }

    protected boolean isGreaterThan(int indexFirst, int indexSecond) {
        this.comparisons++;
        return this.collection[indexFirst].compareTo(this.collection[indexSecond]) > 0;
    }

    protected boolean isGreaterThanOrEqual(int indexFirst, int indexSecond) {
        this.comparisons++;
        return this.collection[indexFirst].compareTo(this.collection[indexSecond]) >= 0;
    }

    protected boolean isLessThan(int indexFirst, int indexSecond) {
        this.comparisons++;
        return this.collection[indexFirst].compareTo(this.collection[indexSecond]) < 0;
    }

    protected boolean isLessThanOrEqual(int indexFirst, int indexSecond) {
        this.comparisons++;
        return this.collection[indexFirst].compareTo(this.collection[indexSecond]) <= 0;
    }

    protected void swap(int indexFirst, int indexSecond) {
        this.swaps++;
        E holdValue = this.collection[indexFirst];
        this.collection[indexFirst] = this.collection[indexSecond];
        this.collection[indexSecond] = holdValue;
    }

    protected abstract void sortCollection();

    public abstract String getName();
}
