package be.ld.sorting.algorithm;

import java.util.ArrayList;
import java.util.List;

public class ShellSort<E extends Comparable<E>> extends SortingAlgorithm<E> {
    private final int[] CUIRA_GAP_SEQUENCE = {1, 4, 10, 23, 57, 132, 301, 701};
    private int[] gaps;

    @Override
    public void sortCollection() {
        generateGaps(this.collection.length);

        for (Integer gap : gaps) {
            for (int i = gap; i < this.collection.length; i++) {
                for (int j = i; j >= gap; j -= gap) {
                    if (isLessThan(j, j - gap)) {
                        swap(j, j - gap);
                    } else {
                        break;
                    }
                }
            }
        }
    }

    private void generateGaps(int size) {
        List<Integer> gaps = new ArrayList<>();

        int counter = 1;
        int newGap = 1;

        while (newGap < size) {
            gaps.add(newGap);
            if (counter < CUIRA_GAP_SEQUENCE.length) {
                newGap = CUIRA_GAP_SEQUENCE[counter++];
            } else {
                newGap = (int) (2.25 * newGap);
            }
        }

        this.gaps = gaps.stream().mapToInt(Integer::intValue).toArray();
    }

    @Override
    public String getName() {
        return "Shellsort";
    }
}
