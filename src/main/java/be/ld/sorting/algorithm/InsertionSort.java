package be.ld.sorting.algorithm;

public class InsertionSort<E extends Comparable<E>> extends SortingAlgorithm<E> {
    @Override
    public void sortCollection() {
        for (int i = 1; i < this.collection.length; i++) {
            for (int j = i; j > 0; j--) {
                if (isLessThan(j, j-1)) {
                    swap(j, j-1);
                } else {
                    break;
                }
            }
        }
    }

    @Override
    public String getName() {
        return "Insertion sort";
    }
}
